// Beta Version
// Owned by PromethiumEX LLC

#ifndef __PSEC_H__
#define __PSEC_H__
#include "../headers/psec.h"
#endif

int main(int argc, char* argv[]) {

#if 0
    unsigned char customerBankAccount[13];

    memset_s(customerBankAccount,sizeof(customerBankAccount),0x00,sizeof(customerBankAccount)*sizeof(unsigned char));

    cout << " :: Welcome to the PromethiumEx Demo :: " << endl;
    cout << "Please enter your 10-12 digit bank account number: " << endl;
    cin >> customerBankAccount;

    while (true) {
        if ( strnlen_s( customerBankAccount,sizeof(customerBankAccount)) < 10 ) {
            cout << "You have entered less than 10 digits. Please try again" << endl;
            cin >> customerBankAccount;
        }
        
        else if ( strnlen_s( customerBankAccount,sizeof(customerBankAccount)) > 12 ) {
            cout << "You have entered more than 12 digits. Please try again" << endl;
            cin >> customerBankAccount;
        }

        else if ( parseBankAccount(customerBankAccount, 12) ) {
            printf("Successful input!\n");
            break;
        }
        
        else  {
            cout << "The bank account you have entered is incorrect. Please try again!" << endl;
            cin >> customerBankAccount;
        }
    }

    if ( getAccountBalanceIfValid(customerBankAccount) ) {
        cout << "Validate Banking Working";
    } else {
        cout << "False from validate banking";
    }
#endif

#if 0

    char * json_resp = (unsigned char*)calloc(1025,sizeof(unsigned char));

    const size_t json_resp_size = 1025;

    createPublicToken(json_resp,json_resp_size);

    printf("createPublicToken reponse:%s\n",json_resp);

    unsigned char * resp = NULL;

    const size_t resp_size = 1025;

    exchangePublicForAccessToken(json_resp,json_resp_size,&resp,resp_size);

    free(json_resp);

#endif

    if ( sodium_init() < 0 )
    {
		fprintf(stderr,"Error: Failed to initialize LibSodium.\nAborting.\n");

		exit(0);
    }

    fill_base64_table();

    fill_base64_decode_table();

//    enc_string_to_file("/tmp/plaid.txt",1025);

    unsigned char * secret = NULL;

    size_t secret_size = 0;

    dec_file_to_string("/tmp/plaid.txt",1025*sizeof(unsigned char),&secret,&secret_size);

    sodium_mprotect_readwrite(secret);
   
    secret_size = strnlen_s(secret,1025*sizeof(unsigned char));

    printf("%s",secret);

    sodium_memzero(secret,secret_size);

    sodium_free(secret);  

    return 0;
}
