#ifndef __PSEC_H__
#define __PSEC_H__
#include "../headers/psec.h"
#endif

void link_token_create(unsigned char * response,size_t response_size,unsigned char * client_secret,size_t client_secret_size)
{
	memset_s(response,response_size,0x00,response_size);

	const size_t new_user_id_size = 33;

	unsigned char new_user_id[new_user_id_size]; // 256 bits of entropy

	memset_s(new_user_id,new_user_id_size,0x00,new_user_id_size);

	randombytes_buf(new_user_id,new_user_id_size-1);
	
	unsigned char json_request[1025];


	memset_s(json_request,sizeof(json_request),0x00,sizeof(json_request));

	size_t concat_size
	
	=
	
	strncat_s(
			json_request
			
			,
			
			sizeof(json_request)
			
			,
			
			"\"user\": { \"client_user_id\": \"\0"
			
			,
			
			strnlen_s("\"user\": { \"client_user_id\": \"\0",1025)

			,


			strnlen_s("\"user\": { \"client_user_id\": \"\0",1025)
			
		);

	if ( concat_size == 0 )
	{
		fprintf(stderr,"Error: Setting json_request for client_user_id in link_token_create() failed.\nAborting.\n");

		exit(0);
	}

	concat_size
		
	=
	
	strncat_s(
			json_request
			
			,
			
			sizeof(json_request)
			
			,
			
			"\"},\0"
			
			,
			
			strnlen_s("\"},\0",1025)

			,


			strnlen_s("\"},\0",1025)
			
		);
	
	if ( concat_size == 0 )
	{
		fprintf(stderr,"Error: Setting json_request for client_user_id in link_token_create() failed.\nAborting.\n");

		exit(0);
	}
	
	concat_size
		
	=
	
	strncat_s(
			json_request
			
			,
			
			sizeof(json_request)
			
			,
			
			"\"client_name\": \0"
			
			,
			
			strnlen_s("\"client_name\": \0",1025)

			,
			
			strnlen_s("\"client_name\": \0",1025)
			
		);
	
	if ( concat_size == 0 )
	{
		fprintf(stderr,"Error: Setting json_request for client_user_id in link_token_create() failed.\nAborting.\n");

		exit(0);
	}

	concat_size
		
	=
	
	strncat_s(
			json_request
			
			,
			
			sizeof(json_request)
			
			,
			
			"\"PromethiumEx\",\0"
			
			,
			
			strnlen_s("\"PromethiumEx\",\0",1025)

			,
			
			strnlen_s("\"PromethiumEx\",\0",1025)
			
		);
	
	if ( concat_size == 0 )
	{
		fprintf(stderr,"Error: Setting json_request for client_user_id in link_token_create() failed.\nAborting.\n");

		exit(0);
	}

	concat_size
		
	=
	
	strncat_s(
			json_request
			
			,
			
			sizeof(json_request)
			
			,
			
			"\"products\": [\"auth\",\"transactions\",\"identity\",\"transfer\"]\0"
			
			,
			
			strnlen_s("\"products\": [\"auth\",\"transactions\",\"identity\",\"transfer\"]\0",1025)

			,


			strnlen_s("\"products\": [\"auth\",\"transactions\",\"identity\",\"transfer\"]\0",1025)
			
		);
	
	if ( concat_size == 0 )
	{
		fprintf(stderr,"Error: Setting json_request for products in link_token_create() failed.\nAborting.\n");

		exit(0);
	}

	concat_size
		
	=
	
	strncat_s(
			json_request
			
			,
			
			sizeof(json_request)
			
			,
			
			"\"language\": \"en\""
			
			,
			
			strnlen_s("\"language\": \"en\"",1025)

			,
			
			strnlen_s("\"language\": \"en\"",1025)
			
		);

	if ( concat_size == 0 )
	{
		fprintf(stderr,"Error: Setting json_request for language in link_token_create() failed.\nAborting\n");
		exit(0);
	}
	
	concat_size
		
	=
	
	strncat_s(
			json_request
			
			,
			
			sizeof(json_request)
			
			,
			
			"\"country_codes\": [\"US\"]"
			
			,
			
			strnlen_s("\"country_codes\": [\"US\"]",1025)

			,
			
			strnlen_s("\"country_codes\": [\"US\"]",1025)
			
		);

	if ( concat_size == 0 )
	{
		fprintf(stderr,"Error: Setting json_request for country_codes in link_token_create() failed.\nAborting.\n");
		exit(0);
	}
	
	concat_size
		
	=
	
	strncat_s(
			json_request
			
			,
			
			sizeof(json_request)
			
			,
			
			client_secret
			
			,
			
			client_secret_size

			,
			
			client_secret_size
			
		);

	if ( concat_size == 0 )
	{
		fprintf(stderr,"Error: Setting json_request for country_codes in link_token_create() failed.\nAborting.\n");
		exit(0);
	}
	
	// Concatenate client_id + secret lastly
}
