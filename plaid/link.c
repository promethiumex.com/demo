#ifndef __PSEC_H__
#define __PSEC_H__
#include "../headers/psec.h"
#endif

void buf_to_string(unsigned char * str,const size_t str_size,const unsigned char * buf,const size_t bufsize)
{
	if ( str_size >= (2*bufsize) )
	{
		fprintf(stderr,"Error: str in buf_to_string() not large enough to hold string.\nAborting.\n");

		exit(0);
	}

	unsigned char hextable[17] = { '0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f',0x00};

	for ( size_t i = 0; (i < bufsize) ; )
	{
		str[i] = hextable[( buf[i] / 16 )];

		i++;

		str[i] = hextable[( buf[i] % 16 )];
	
		i++;

	}

}


void link_token_create(unsigned char ** response,size_t * response_size,unsigned char * client_secret,size_t client_secret_size)
{
	if ( response == NULL || (*response) == NULL )
	{
		fprintf(stderr,"Error: response or (*response) == NULL.\nAborting.\n");
		
		exit(0);
	}
	
	memset_s((*response),(*response_size),0x00,(*response_size));

	const size_t new_user_id_size = 17, new_user_id_str_size = 33;

	unsigned char new_user_id[new_user_id_size]; // 256 bits of entropy

	unsigned char new_user_id_str[33];

	memset_s(new_user_id_str,new_user_id_str_size,0x00,new_user_id_str_size);
	
	memset_s(new_user_id,new_user_id_size,0x00,new_user_id_size);

	randombytes_buf(new_user_id,new_user_id_size-1);

	buf_to_string(new_user_id_str,new_user_id_str_size,new_user_id,new_user_id_size);
	
	unsigned char * json_request = (unsigned char*)sodium_malloc(1025*sizeof(unsigned char));

	const size_t json_request_size = 1025*sizeof(unsigned char);

	memset_s(json_request,json_request_size,0x00,json_request_size);

	size_t concat_size = 0;
	
	concat_size
	
	=
	
	strncat_s(
			json_request
			
			,
			
			json_request_size
			
			,
			
			"{\0"
			
			,
			
			strnlen_s("{\0",1025)

			,


			strnlen_s("{\0",1025)
			
		);

	if ( concat_size == 0 )
	{
		fprintf(stderr,"Error: Setting json_request for '{' in link_token_create() failed.\nAborting.\n");

		exit(0);
	}


	concat_size
	
	=
	
	strncat_s(
			json_request
			
			,
			
			json_request_size
			
			,
			
			"\"user\": { \"client_user_id\": \"\0"
			
			,
			
			strnlen_s("\"user\": { \"client_user_id\": \"\0",1025)

			,


			strnlen_s("\"user\": { \"client_user_id\": \"\0",1025)
			
		);

	if ( concat_size == 0 )
	{
		fprintf(stderr,"Error: Setting json_request for client_user_id in link_token_create() failed.\nAborting.\n");

		exit(0);
	}
	
	concat_size
	
	=
	
	strncat_s(
			json_request
			
			,
			
			json_request_size
			
			,
			
			new_user_id_str
			
			,
			
			strnlen_s(new_user_id_str,new_user_id_str_size)

			,


			strnlen_s(new_user_id_str,new_user_id_str_size)
			
		);

	if ( concat_size == 0 )
	{
		fprintf(stderr,"Error: Setting json_request for client_user_id in link_token_create() failed.\nAborting.\n");

		exit(0);
	}

	concat_size
		
	=
	
	strncat_s(
			json_request
			
			,
			
			json_request_size
			
			,
			
			"\"},\0"
			
			,
			
			strnlen_s("\"},\0",1025)

			,


			strnlen_s("\"},\0",1025)
			
		);
	
	if ( concat_size == 0 )
	{
		fprintf(stderr,"Error: Setting json_request for client_user_id in link_token_create() failed.\nAborting.\n");

		exit(0);
	}
	
	concat_size
		
	=
	
	strncat_s(
			json_request
			
			,
			
			json_request_size
			
			,
			
			"\"client_name\": \0"
			
			,
			
			strnlen_s("\"client_name\": \0",1025)

			,
			
			strnlen_s("\"client_name\": \0",1025)
			
		);
	
	if ( concat_size == 0 )
	{
		fprintf(stderr,"Error: Setting json_request for client_user_id in link_token_create() failed.\nAborting.\n");

		exit(0);
	}

	concat_size
		
	=
	
	strncat_s(
			json_request
			
			,
			
			json_request_size
			
			,
			
			"\"PromethiumEx\",\0"
			
			,
			
			strnlen_s("\"PromethiumEx\",\0",1025)

			,
			
			strnlen_s("\"PromethiumEx\",\0",1025)
			
		);
	
	if ( concat_size == 0 )
	{
		fprintf(stderr,"Error: Setting json_request for client_user_id in link_token_create() failed.\nAborting.\n");

		exit(0);
	}

	concat_size
		
	=
	
	strncat_s(
			json_request
			
			,
			
			json_request_size
			
			,
			
			"\"products\": [\"auth\",\"transactions\"],\0"
			
			,
			
			strnlen_s("\"products\": [\"auth\",\"transactions\"],\0",1025)

			,


			strnlen_s("\"products\": [\"auth\",\"balance\",\"transactions\"],\0",1025)
			
		);
	
	if ( concat_size == 0 )
	{
		fprintf(stderr,"Error: Setting json_request for products in link_token_create() failed.\nAborting.\n");

		exit(0);
	}

	concat_size
		
	=
	
	strncat_s(
			json_request
			
			,
			
			json_request_size
			
			,
			
			"\"language\": \"en\","
			
			,
			
			strnlen_s("\"language\": \"en\",",1025)

			,
			
			strnlen_s("\"language\": \"en\",",1025)
			
		);

	if ( concat_size == 0 )
	{
		fprintf(stderr,"Error: Setting json_request for language in link_token_create() failed.\nAborting\n");
		exit(0);
	}
	
	concat_size
		
	=
	
	strncat_s(
			json_request
			
			,
			
			json_request_size
			
			,
			
			"\"country_codes\": [\"US\"],"
			
			,
			
			strnlen_s("\"country_codes\": [\"US\"],",1025)

			,
			
			strnlen_s("\"country_codes\": [\"US\"],",1025)
			
		);

	if ( concat_size == 0 )
	{
		fprintf(stderr,"Error: Setting json_request for country_codes in link_token_create() failed.\nAborting.\n");
		exit(0);
	}
	
	concat_size
		
	=
	
	strncat_s(
			json_request
			
			,
			
			json_request_size
			
			,
			
			client_secret
			
			,
			
			client_secret_size

			,
			
			client_secret_size
			
		);

	if ( concat_size == 0 )
	{
		fprintf(stderr,"Error: Setting json_request for client_id and client_secret in link_token_create() failed.\nAborting.\n");

		exit(0);
	}
	
	concat_size
	
	=
	
	strncat_s(
			json_request
			
			,
			
			json_request_size
			
			,
			
			"}\0"
			
			,
			
			strnlen_s("}\0",1025)

			,


			strnlen_s("}\0",1025)
			
		);
	
	if ( concat_size == 0 )
	{
		fprintf(stderr,"Error: Setting json_request for '}' in link_token_create() failed.\nAborting.\n");

		exit(0);
	}
	
	// Concatenate client_id + secret lastly
	
	const unsigned char method[] = "POST\0";

	const unsigned char url[] = "https://development.plaid.com/link/token/create\0";

	const unsigned char * file_path = NULL;

	const unsigned char * headers[] =

	{
		"Content-Type: application/json\0"

		,

		NULL
	};

	const unsigned char * payload = json_request;
	
	// ** response is already defined in the 
	// function signature
	
	const size_t num_headers = 1;
	
	(*response_size) = 1025;

	http_request(method,url,headers,num_headers,payload,file_path,response,(*response_size));

	sodium_memzero(json_request,1025*sizeof(unsigned char));
	
	sodium_free(json_request);
}
