EXECS=/tmp/main.out

CC=gcc

LIBRARIES=-lcurl -lsodium -ljemalloc

COMPILER_OPTIONS=-fpermissive -Wwrite-strings -w

PLAID=plaid/link.c plaid/main.c plaid/plaid.c plaid/json.c

CURL=curl/http.c

PSEC=psec/base64.c psec/boyer_moore.c psec/getpass.c psec/sstring.c psec/crypto.c

#MYFILES=plaid/main.cpp psec/boyer_moore.c psec/sstring.c plaid/plaid.cpp plaid/json.cpp curl/http.cpp

HEADERS=headers/psec.h 

all: ${EXECS}

${EXECS}: ${MYFILE}
	${CC} -O2 -o ${EXECS} ${PLAID} ${CURL} ${PSEC} ${HEADERS} --std=c11 ${LIBRARIES} ${COMPILER_OPTIONS}

clean:
	rm -f ${EXECS}
