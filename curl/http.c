#ifndef __PSEC_H__
#define __PSEC_H__
#include "../headers/psec.h"
#endif

size_t write_callback(unsigned char * raw_download_contents,size_t size,size_t nmemb,unsigned char * download_contents_save)
{
	size_t i = 0;

	for ( ; i < ( size * nmemb ) ; i++ )
	{
		download_contents_save[i] = raw_download_contents[i];
	}

	return i;
}

// User is responsible for freeing dynamic memory in response

void http_request(const unsigned char * method,const unsigned char * url,const unsigned char * headers[],const size_t num_headers,const unsigned char * payload,const unsigned char * file_path,unsigned char ** response,const size_t response_size)
{
	unsigned char * json_string = NULL;

	CURL * curl = curl_easy_init();

	if ( curl == NULL )
	{
		fprintf(stderr,"Error: Failed to initialize CURL * object in http_request(). Aborting\n");

		exit(0);
	}

	CURLcode res;

	memset_s((unsigned char*)&res,sizeof(CURLcode),0x00, sizeof(CURLcode));

	struct curl_slist * list = NULL;

	if ( strnstr_s( (unsigned char*)method,16,(unsigned char*)"POST") != NULL )
	{
		for ( size_t i = 0 ; i < num_headers ; i++ )
		{
			list = curl_slist_append(list,headers[i]);
		}

		curl_easy_setopt(curl,CURLOPT_CUSTOMREQUEST,method);

		curl_easy_setopt(curl,CURLOPT_HTTPHEADER,list);

// Beginning of file payload handling
		
		if ( file_path != NULL )
		{

			FILE * in = NULL;

			if ( ( in = fopen(file_path,"r+") ) == NULL )
			{
				fprintf(stderr,"Error: Failed to open json file in http_request(). Aborting\n");

				exit(0);
			}

			if ( fseek(in,0L,SEEK_END) != 0 )
			{
				fprintf(stderr,"Error: Failed to seek end of file in http_request(). Aborting\n");

				exit(0);
			}

			const size_t json_string_size = ftell(in);

			rewind(in);

			json_string = (unsigned char*)calloc(json_string_size+1,sizeof(unsigned char));

			if ( json_string == NULL )
			{
				fprintf(stderr,"Error: Ran out of heap memory in createToken(). Aborting\n");

				exit(0);
			}

			file_to_json_string(json_string,json_string_size,in);

			rewind(in);

			fclose(in);

//			printf("%s\n",json_string);

			curl_easy_setopt(curl,CURLOPT_POSTFIELDS,json_string);
	

// Ending of File handling

		}

// payload preloaded through payload string in function call

		else
		{
			curl_easy_setopt(curl,CURLOPT_POSTFIELDS,payload);
			
		}
	
		if ( (*response) == NULL )
		{
			(*response) = (unsigned char*)calloc(response_size,sizeof(unsigned char));
		}

		if ( (*response) == NULL )
		{
			fprintf(stderr,"Error: Ran out of heap memory in http_request(). Aborting\n");

			exit(0);
		}

		curl_easy_setopt(curl,CURLOPT_WRITEFUNCTION,write_callback);

		curl_easy_setopt(curl,CURLOPT_WRITEDATA,(*response));

	}
	
	curl_easy_setopt(curl, CURLOPT_URL,url);

	res = curl_easy_perform(curl);

	curl_easy_cleanup(curl);

	if ( json_string != NULL )
	{
		free(json_string);
	}

}

