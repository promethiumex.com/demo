#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct my_struct_tag
{
	pthread_mutex_t mutex;

	int value;

}my_struct_t;

int main(int argc,char * argv[])
{
	my_struct_t * data;

	int status;

	data = malloc(sizeof(my_struct_t));

	if ( data == NULL )
	{
		fprintf(stderr,"Error: Failed to allocate data in main().\nAborting.\n");

		exit(1);
	}

	status = pthread_mutex_init(&data->mutex,NULL);

	if ( status != 0 )
	{
		fprintf(stderr,"Error: Failed to call pthread_mutex_init() in main().\nAborting.\n");

		exit(1);
	}

	status = pthread_mutex_destroy(&data->mutex);

	if ( status != 0 )
	{
		fprintf(stderr,"Error: Failed to destroy mutex in main().\nAborting.\n");
		
		exit(1);
	}

	free(data);

	return status;
}
