void err_abort(unsigned char * text)
{
	do
	{
		fprintf(stderr,"Error: %s at \"%s\":%d: %s\n",text,__FILE__,__LINE__,strerror(errno));

		abort();
	}while(0);
}
