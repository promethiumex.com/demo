#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>

void * thread_routine(void * arg)
{
	return arg;
}

int main(int argc,char * argv[])
{
	pthread_t thread_id;

	void * thread_result;

	int status = pthread_create(&thread_id,NULL,thread_routine,NULL);

	if ( status != 0 )
	{
		fprintf(stderr,"Error: Failed to create thread\n");

		exit(1);
	}

	status = pthread_join(thread_id,&thread_result);

	if ( status != 0 )
	{
		fprintf(stderr,"Error: Failed to join thread\n");

		exit(1);
	}

	if ( thread_result == NULL )
	{
		return 0;
	}

	else
	{
		return 1;
	}
}
