#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

#define SPIN 10000000

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

long counter;

time_t end_time;

void * counter_thread(void * arg)
{
	int status;

	int spin;

	while ( time(NULL) < end_time )
	{
		status = pthread_mutex_lock(&mutex);

		if ( status != 0 )
		{
			fprintf(stderr,"Error: Failed lock mutex in main().\nAborting.\n");

			exit(1);
		}

		for ( spin = 0; spin < SPIN ; spin++ )
		{
			counter++;
		}

		status = pthread_mutex_unlock(&mutex);

		if ( status != 0 )
		{
			fprintf(stderr,"Error: Failed to unlock mutex in main().\nAborting.\n");

			exit(1);
		}

		sleep(1);
	}

	printf("Counter in counter_thread is %#lx\n",counter);

	return NULL;
}

void * monitor_thread(void *arg)
{
	int status;

	int misses = 0;

	while ( time(NULL) < end_time )
	{
		sleep(3);

		status = pthread_mutex_trylock(&mutex);

		if ( status != EBUSY )
		{
			if ( status != 0 )
			{
				fprintf(stderr,"Error: Trylock mutex failed in monitor_thread().\nAborting.\n");

				exit(1);
			}

			printf("Counter is %ld\n",counter/SPIN);

			status = pthread_mutex_unlock(&mutex);

			if ( status != 0 )
			{
				fprintf(stderr,"Error: Unlock mutex failed in monitor_thread().\nAborting.\n");
			}

		}
		
		else
		{
			misses++;
		}
	}

	printf("Monitor thread missed update %d time.\n",misses);

	return NULL;
}

int main(int argc,char * argv[])
{
	pthread_t counter_thread_id;

	pthread_t monitor_thread_id;

	end_time = time(NULL) + 60;

	int status = pthread_create(&counter_thread_id,NULL,counter_thread,NULL);
	

	if ( status != 0 )
	{
		fprintf(stderr,"Error: Failed to create counter thread in main().\nAborting.\n");

		exit(1);
	}

	status = pthread_create(&monitor_thread_id,NULL,monitor_thread,NULL);

	if ( status != 0 )
	{
		fprintf(stderr,"Error: Failed to create monitor thread in main().\nAborting.\n");

		exit(1);
	}

	status = pthread_join(counter_thread_id,NULL);
	
	if ( status != 0 )
	{
		fprintf(stderr,"Error: Failed to join counter thread in main().\nAborting.\n");

		exit(1);
	}

	return 0;
}


