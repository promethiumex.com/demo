import _thread
import time
import signal


raw_passwd = "The"

def handler(signum,frame):
   global raw_passwd

   del raw_passwd

   print("Exiting process")

   exit(0)

signal.signal(signal.SIGINT,handler)

def sleeper():
    while 1:
        time.sleep(3)
        print("Testing")

try:
   _thread.start_new_thread(sleeper,())
except:
   print("Error: Failed to initiate thread")


while 1:
    time.sleep(5)
    print("Looping")
    print("raw_passwd ?= None",raw_passwd == None)
