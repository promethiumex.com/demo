#include <stdio.h>
#include <time.h>
#include "mongoose.h"

static struct mg_mgr mgr;

int stop = 0;

void signal_int(int sig)
{
	stop = 1;
}

static void cb(struct mg_connection *c, int ev, void *ev_data, void *fn_data) {
  if (ev == MG_EV_READ) {
    mg_send(c, c->recv.buf, c->recv.len);     // Echo received data back
    memset((c->recv).buf,0x00,(c->recv).size);

   // mg_iobuf_del(&c->recv, 0, c->recv.len);   // And discard it
  }
}

int main(int argc, char *argv[]) {
  struct mg_mgr mgr;
  mg_mgr_init(&mgr);                                // Init manager
  mg_listen(&mgr, "tcp://0.0.0.0:1234", cb, &mgr);  // Setup listener
  signal(SIGINT,signal_int);
  for (;;)
  {
	  mg_mgr_poll(&mgr, 1000);                 // Event loop
	  
	  if ( stop == 1 ) break;
  }

  struct mg_connection * c = 0x00;

  clock_t begin;

  clock_t time_spent = 0.00;

  begin = clock();

  printf("Made it.\n");

  size_t i = 0;

#if 0
  // The SIGINT (Ctrl+C) causes main thread
  // to break out of infinite loop.
  // No new connection requests are accept()ed
  // from this point onwards.
  // Before officially shutting down server,
  // in-progress requests have 30 seconds to
  // complete and no new connections are accepted.
#endif

  while ( i < 30  )
  {
	  printf("iteration: %zu\n",i);

	  for ( c = (&mgr)->conns ; c != 0x00 ; c = c->next )
	  {
		c->is_listening = 0;
	  }

	  mg_mgr_poll(&mgr,1000);

	  i++;
  }


  mg_mgr_free(&mgr);                                // Cleanup

  return 0;
}

