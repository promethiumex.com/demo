#include <iostream>
#include <string.h>
#include <string>
#include <curl/curl.h>


size_t WriteCallback(unsigned char *contents, size_t size, size_t nmemb,unsigned char *userp)
{
#if 0
    ((std::string*)userp)->append((char*)contents, size * nmemb);
    return size * nmemb;
#endif

    size_t i = 0;


    for ( ; i < (size*nmemb); i++ )
    {
	userp[i] = contents[i];
    }

    return 0;    
}

int main(void)
{
  CURL *curl;
  CURLcode res;
  unsigned char * readBuffer = (unsigned char*)calloc(9182,sizeof(unsigned char));

  curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "https://duckduckgo.com");
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, readBuffer);
    res = curl_easy_perform(curl);
    curl_easy_cleanup(curl);

    printf("readBuffer:%s\n",readBuffer);

//    std::cout << readBuffer << std::endl;
  }

  free(readBuffer);

  return 0;
}
