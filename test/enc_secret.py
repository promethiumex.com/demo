import pysodium
import binascii
import sys
from getpass import getpass

salt = binascii.unhexlify(b'00000000000000000000000000000000')

print("Enter raw password in the line below:")

raw_key = getpass()

key = pysodium.crypto_pwhash(pysodium.crypto_auth_KEYBYTES,raw_key,salt,pysodium.crypto_pwhash_argon2id_OPSLIMIT_INTERACTIVE,pysodium.crypto_pwhash_argon2id_MEMLIMIT_INTERACTIVE,pysodium.crypto_pwhash_ALG_ARGON2ID13)

#print(key)

ad = binascii.unhexlify(b'00000000000000000000000000000000')

nonce = binascii.unhexlify(b"000000000000000000000000000000000000000000000000")

print("Enter secret message starting from the line below:")

msg = getpass()

y = pysodium.crypto_aead_xchacha20poly1305_ietf_encrypt(bytes(msg,'utf-8'),ad,nonce,key)

print(type(y))

z = pysodium.crypto_aead_xchacha20poly1305_ietf_decrypt(y,ad,nonce,key)

#print(y)

#print(z)

# Converts encrypted output to a hex 'bytes' array and finally to an 'str'

hexified = binascii.hexlify(y).decode()

print(hexified)

print(type(hexified))
