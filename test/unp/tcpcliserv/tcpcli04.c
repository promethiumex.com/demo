#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <netdb.h>
#include <errno.h>
#include <syslog.h>
#include <arpa/inet.h>
#include <sys/uio.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/un.h>
#ifndef __PSEC_H__
#define __PSEC_H__
#include "../../../headers/psec.h"
#endif

void str_cli(FILE * fp,int sockfd)
{
	unsigned char sendline[1025], recvline[1025];

	while ( fgets(sendline,1025,fp) != NULL )
	{
		write(sockfd,sendline,strnlen_s(sendline,1024));

		sleep(1);
		
		write(sockfd,sendline,strnlen_s(sendline,1024));

		if ( read(sockfd,recvline,1025) == 0 )
		{
			fprintf(stderr,"Error: Server terminate prematurely.\nAborting.\n");

			exit(1);	
		}

		fputs(recvline,stdout);
	}
}

int main(int argc,char*argv[])
{
	int i = 0, sockfd[5];

	struct sockaddr_in servaddr;

	if ( argc != 2 )
	{
		fprintf(stderr,"Error: Required one CLI argument.\nAborting.\n");

		exit(1);
	}

	for (i = 0; i < 5 ; i++ )
	{
		sockfd[i] = socket(AF_INET,SOCK_STREAM,0);

		memset_s((unsigned char*)&servaddr,sizeof(servaddr),0x00,sizeof(servaddr));

		servaddr.sin_family = AF_INET;

		servaddr.sin_port = htons(9877);

		inet_pton(AF_INET,argv[1],&servaddr.sin_addr);

		connect(sockfd[i],(struct sockaddr*)&servaddr,sizeof(servaddr));

	}
	
	str_cli(stdin,sockfd[0]);

	exit(0);

	return 0;
}
