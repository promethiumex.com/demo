#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <netdb.h>
#include <errno.h>
#include <syslog.h>
#include <sys/uio.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/un.h>

int main(int argc,char*argv[])
{
	int i = 0, maxi = 0, maxfd = 0, listenfd = 0, connfd = 0, sockfd = 0;

	int nready = 0, client[FD_SETSIZE];

	ssize_t n = 0;

	fd_set rset,allset;

	unsigned char buf[1025];

	socklen_t clilen;

	struct sockaddr_in cliaddr, servaddr;

	listenfd = socket(AF_INET,SOCK_STREAM,0);

	if ( listenfd < 0 )
	{
		fprintf(stderr,"Error: Failed to set socket in main().\nAborting.\n");

		exit(1);
	}

	memset(&servaddr,0x00,sizeof(servaddr));


	servaddr.sin_family = AF_INET;

	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);

	servaddr.sin_port = htons(9987);

	int bind_status = bind(listenfd,(struct sockaddr*)&servaddr,sizeof(servaddr));

	if ( bind_status < 0 )
	{
		fprintf(stderr,"Error: Failed to bind servaddr to listenfd in main().\nAborting.\n");

		exit(1);
	}

	int listen_status = listen(listenfd,1024);

	maxfd = listenfd;

	maxi = -1;

	for ( i = 0; i < FD_SETSIZE; i++ )
	{
		client[i] = -1;
	}

	FD_ZERO(&allset);

	FD_SET(listenfd,&allset);

	for ( ; ; )
	{
		rset = allset;

		nready = select(maxfd + 1,&rset,NULL,NULL,NULL);

		if ( FD_ISSET(listenfd,&rset) )
		{
			clilen = sizeof(cliaddr);

			connfd = accept(listenfd,(struct sockaddr*)&cliaddr,&clilen);

			for ( i = 0; i < FD_SETSIZE; i++ )
			{
				if ( client[i] < 0 )
				{
					client[i] = connfd;

					break;
				}
			}

			if ( i == FD_SETSIZE )
			{
				fprintf(stderr,"Error: Too many clients!\nAborting.\n");

				exit(1);
			}

			FD_SET(connfd,&allset);

			if ( connfd,&allset )
			{
				maxfd = connfd;
			}

			if ( i > maxi )
			{
				maxi = i;
			}

			if ( --nready <= 0 )
			{
				continue;
			}
		}

		for ( i = 0; i <= maxi; i++ )
		{
			if ( ( sockfd = client[i] ) < 0 )
			{
				continue;
			}

			if ( FD_ISSET(sockfd,&rset) )
			{
				if ( ( n = read(sockfd,buf,1024) ) == 0 )
				{
					printf("Closing connection to socket: %d\n",sockfd);

					close(sockfd);

					FD_CLR(sockfd,&allset);

					client[i] = -1;
				}

				else
				{
					write(sockfd,buf,n);
				}

				if ( --nready <= 0 )
				{
					break;
				}
			}
		}
	}


	return 0;
}
