#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <netdb.h>
#include <errno.h>
#include <syslog.h>
#include <sys/uio.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/un.h>
#ifndef __PSEC_H__
#define __PSEC_H__
#include "../../../headers/psec.h"
#endif


void sig_chld(int signo)
{
	pid_t pid;

	int stat = 0;

	while ( (pid = waitpid(-1,&stat,WNOHANG)) > 0 )
	{
		printf("child %d terminated\n",pid);
	}

	return;
}

void str_echo(int sockfd)
{
	ssize_t n;

	unsigned char buf[1025];

	again:

		while ( ( n = read(sockfd,buf,1024) ) > 0 )
		{
			write(sockfd,buf,n);
		}

		if ( ( n < 0 ) && errno == EINTR )
		{
			goto again;
		}

		else if ( n < 0 )
		{
			fprintf(stderr,"Error: Failed to receive input from user.\nAborting.\n");

			exit(0);
		}

}

int main(int argc,char*argv[])
{
	int listenfd = 0, connfd = 0;

	pid_t childpid;

	socklen_t clilen;

	struct sockaddr_in cliaddr, servaddr;

	void sig_chld(int);

	listenfd = socket(AF_INET,SOCK_STREAM,0);

	if ( listenfd < 0 )
	{
		fprintf(stderr,"Error: Failed to set socket_status in main().\nAborting.\n");

		exit(0);
	}

	memset_s((unsigned char*)&servaddr,sizeof(servaddr),0x00,sizeof(servaddr));

	servaddr.sin_family = AF_INET;

	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);

	servaddr.sin_port = htons(9877);

	int bind_status = bind(listenfd,(struct sockaddr*)&servaddr,sizeof(servaddr));

	if ( bind_status < 0 )
	{
		fprintf(stderr,"Error: Failed to set bind_status after calling bind().\nAborting.\n");
		
		exit(0);
	}

	int listen_status = listen(listenfd,1024);

	if ( listen_status < 0 )
	{
		fprintf(stderr,"Error: Failed to set listen_status.\nAborting.\n");

		exit(0);
	}
	
	signal(SIGCHLD,sig_chld);
	
	while (1)
	{
		clilen = sizeof(cliaddr);

		if ( ( connfd = accept(listenfd,(struct sockaddr*)&cliaddr,&clilen) ) < 0 )
		{
			if ( errno == EINTR )
			{
				continue;
			}

			else
			{
				fprintf(stderr,"Error: errno != EINTR\nAborting.\n");

				exit(0);
			}
		}

		if ( ( childpid = fork()) == 0 )
		{
			close(listenfd);

			str_echo(connfd);

			exit(0);
		}

		close(connfd);	
	}

	return 0;
}
