#include <arpa/inet.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <netdb.h>
#include <errno.h>
#include <syslog.h>
#include <sys/uio.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/un.h>



int tcp_listen(const unsigned char * host, const unsigned char * serv,socklen_t * addrlenp)
{
	int listenfd = 0, n = 0;

	const int on = 1;

	struct addrinfo hints, * res, * ressave;

	memset(&hints,0x00,sizeof(struct addrinfo));

	hints.ai_flags = AI_PASSIVE;

	hints.ai_family = AF_UNSPEC;

	hints.ai_socktype = SOCK_STREAM;

	if ( ( n = getaddrinfo(host,serv,&hints,&res) ) != 0 )
	{
		fprintf(stderr,"tcp_listen error for %s, %s: %s\n",host,serv,gai_strerror(n));
		
		exit(1);
	}

	ressave = res;

	do
	{
		listenfd = socket(res->ai_family,res->ai_socktype,res->ai_protocol);

		if ( listenfd < 0 )
		{
			continue;
		}

		setsockopt(listenfd,SOL_SOCKET,SO_REUSEADDR,&on,sizeof(on));

		if ( bind(listenfd,res->ai_addr,res->ai_addrlen) == 0 )
		{
			break;
		}

		close(listenfd);

	} while( ( res = res->ai_next ) != NULL );

	if ( res == NULL )
	{
		fprintf(stderr,"Error: tcp_listen error for %s, %s\n",host,serv);

		exit(1);
	}

	listen(listenfd,1024);

	if ( addrlenp != NULL )
	{
		*addrlenp = res->ai_addrlen;

	}

	freeaddrinfo(ressave);

	return listenfd;
}

unsigned char * sock_ntop(const struct sockaddr * ss, socklen_t sslen)
{
	unsigned char portstr[8];

	unsigned char * str = (unsigned char*)calloc(128,sizeof(unsigned char));

	struct sockaddr_in * sin = (struct sockaddr_in*)ss;

	if ( inet_ntop(AF_INET,&sin->sin_addr,str,sizeof(str)) == NULL )
	{
		return NULL;	
	}

	if ( ntohs(sin->sin_port) != 0 )
	{
		snprintf(portstr,sizeof(portstr),":%d",ntohs(sin->sin_port));

		strncat(str,portstr,sizeof(portstr));
	}

	return str;
}


int main(int argc,char*argv[])
{
	int listenfd = 0, connfd = 0;

	socklen_t len;

	unsigned char buf[1025];

	time_t ticks;

	struct sockaddr_storage cliaddr;

	socklen_t addrlen;

	if ( argc == 2 )
	{
		listenfd = tcp_listen(NULL,argv[1],&addrlen);
	}

	else if ( argc == 3 )
	{
		listenfd = tcp_listen(argv[1],argv[2],&addrlen);
	}

	else
	{
		fprintf(stderr,"Error: usage: daytimetcpsrv2 [ <host> ] <service or port>");

		exit(1);
	}

	for ( ; ; )
	{
		len = sizeof(cliaddr);

		connfd = accept(listenfd,(struct sockaddr*)&cliaddr,&len);

		printf("connection from %s\n",sock_ntop((struct sockaddr*)&cliaddr,len));

		ticks = time(NULL);

		snprintf(buf,sizeof(buf),"%.24s\r\n",ctime(&ticks));

		write(connfd,buf,strnlen(buf,sizeof(buf)-1));

		close(connfd);
	}

	return 0;
}
