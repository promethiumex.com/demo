#include <arpa/inet.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <netdb.h>
#include <errno.h>
#include <syslog.h>
#include <sys/uio.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/un.h>

#if 0
https://github.com/unpbook/unpv13e/blob/master/lib/sock_ntop_host.c: sock_ntop_host
#endif

unsigned char * sock_ntop_host(const struct sockaddr *sa,socklen_t salen)
{
	unsigned char * str = (unsigned char*)calloc(128,sizeof(unsigned char));

	switch (sa->sa_family)
	{
		case AF_INET:
		{
			struct sockaddr_in * sin = (struct sockaddr_in*)sa;

			if ( inet_ntop(AF_INET,&sin->sin_addr,str,sizeof(str)) == NULL )
			{
				return NULL;
			}

			return str;
		}

		case AF_INET6:
		{
			struct sockaddr_in6 * sin6 = (struct sockaddr_in6*)sa;

			if ( inet_ntop(AF_INET6,&sin6->sin6_addr,str,sizeof(str)) == NULL )
			{
				return NULL;
			}

			return str;
		}

		default:
		{
			snprintf(str,sizeof(str),"sock_ntop_host: unknown AF_xxx: %d, len %d",sa->sa_family,salen);
			return str;
		}

	}

	return NULL;
}

int tcp_connect(const unsigned char * host, const unsigned char * serv)
{
	int sockfd = 0, n = 0;

	struct addrinfo hints, * res = 0x00, * ressave = 0x00;

	memset(&hints,0x00,sizeof(struct addrinfo));

	hints.ai_family = AF_UNSPEC;

	hints.ai_socktype = SOCK_STREAM;

	if ( ( n = getaddrinfo(host,serv,&hints,&res) ) != 0 )
	{
		fprintf(stderr,"Error: tcp_connect error for %s, %s: %s",host,serv,gai_strerror(n));

		exit(1);
	}

	ressave = res;

	do
	{
		sockfd = socket(res->ai_family,res->ai_socktype,res->ai_protocol);

		if ( sockfd < 0 )
		{
			continue;
		}

		if ( connect(sockfd,res->ai_addr,res->ai_addrlen) == 0 )
		{
			break;
		}

		close(sockfd);

	}while( ( res = res->ai_next ) != NULL );

	if ( res == NULL )
	{
		fprintf(stderr,"Error: tcp_connect error for %s, %s",host,serv);

		exit(1);
	}

	freeaddrinfo(ressave);

	return sockfd;
}


int main(int argc,char*argv[])
{
	int sockfd = 0, n = 0;

	unsigned char recvline[1024+1];

	socklen_t len;

	struct sockaddr_storage ss;

	if ( argc != 3 )
	{
		fprintf(stderr,"Error: Expected two CLI arguments.\nAborting.\n");

		exit(1);
	}

	sockfd = tcp_connect(argv[1],argv[2]);

	len = sizeof(ss);

	getpeername(sockfd,(struct sockaddr*)&ss,&len);

	unsigned char * sock_ntop_host_str = sock_ntop_host((struct sockaddr*)&ss,len);

	printf("Connected to %s\n",sock_ntop_host_str);

	while ( (n = read(sockfd,recvline,1024) ) > 0 )
	{
		recvline[n] = 0;

		fputs(recvline,stdout);
	}

	free(sock_ntop_host_str);

	exit(0);
}
