#include <stdio.h>
#include <time.h>
#include <netdb.h>
#include <errno.h>
#include <syslog.h>
#include <sys/uio.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/un.h>
#include "../../headers/psec.h"

int main(int argc,unsigned char * argv[])
{
	int listenfd = 0, connfd = 0;

	struct sockaddr_in servaddr;

	unsigned char buf[1025];

	memset_s(buf,1025,0x00,1025);

	time_t ticks;

	memset_s((unsigned char*)&ticks,sizeof(time_t),0x00,sizeof(time_t));

	listenfd = socket(AF_INET,SOCK_STREAM,0);

	if ( listenfd < 0 )
	{
		fprintf(stderr,"Error: Failed to assign socket to listenfd in main().\nAborting.\n");

		exit(0);
	}

	memset_s((unsigned char*)&servaddr,sizeof(struct sockaddr_in),0x00,sizeof(struct sockaddr_in));

	servaddr.sin_family = AF_INET;

	servaddr.sin_addr.s_addr = htonl((INADDR_ANY));

	servaddr.sin_port = htons(13);

	int bind_status = bind(listenfd,(struct sockaddr *)&servaddr,sizeof(servaddr));

	if ( bind_status < 0 )
	{
		fprintf(stderr,"Error: Failed to bind listenfd to servaddr in main().\nAborting.\n");

		exit(0);
	}

	int listen_status = listen(listenfd,1024);

	printf("%d\n",listen_status);

	if ( listen_status < 0 )
	{
		fprintf(stderr,"Error: Failed to set listenfd to listening mode in main().\nAborting\n");

		exit(0);
	}

	while (1)
	{
		printf("Made it.\n");

		printf("LOL\n");

		connfd = accept(listenfd,(struct sockaddr*)NULL,NULL);

		if ( connfd < 0 )
		{
			fprintf(stderr,"Error: Failed to accept socket requests for listenfd in main().\nAborting\n");
			exit(0);
		}
		
		printf("Made it here.\n");

		ticks = time(NULL);

		snprintf(buf,1024*sizeof(unsigned char),"%.24s\r\n",ctime(&ticks));

		ssize_t write_size = write(connfd,buf,strnlen_s(buf,1024*sizeof(unsigned char)));

		if ( write_size < 0 )
		{
			fprintf(stderr,"Error: Failed to write message response to buf from connfd in main().\nAborting.\n");
			exit(0);
		}

		int close_status = close(connfd);

		if ( close_status < 0 )
		{
			fprintf(stderr,"Error: close() failed to close connfd socket in main().\nAborting.\n");

			exit(0);
		}

		printf("Made it below.\n");
	}


	return 0;
}
