#ifndef __PSEC_H__
#define __PSEC_H__
#include "../psec/psec.h"
#endif

int json_file_to_string_test(void)
{
	unsigned char x[256];

	memset_s(x,sizeof(x),0x00,sizeof(x));

	FILE * in = NULL;

	if ( ( in = fopen("../helper/sandbox_public_token.json","r+") ) == NULL )
	{
		fprintf(stderr,"Error: Failed to open file.\nAborting\n");

		return -1;
	}

	size_t file_size = 0;

	if ( fseek(in,0L,SEEK_END) != 0 )
	{
		fprintf(stderr,"Error: Failed to seek end of file.\nAborting\n");

		return -1;
	}


	file_size = ftell(in);
	
	rewind(in);

	file_to_json_string(x,file_size,in);

	for ( size_t i = 0; i < strnlen_s(x,sizeof(x)-1) ; i++ )
	{
		putchar(x[i]);
	}

	fclose(in);

	return 0;
}
