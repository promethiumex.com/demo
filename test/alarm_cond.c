#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <time.h>

typedef struct alarm_tag
{
	struct alarm_tag * link;

	int seconds;

	time_t time;

	unsigned char message[65];
	
} alarm_t;

pthread_mutex_t alarm_mutex = PTHREAD_MUTEX_INITIALIZER;

pthread_cond_t alarm_cond = PTHREAD_COND_INITIALIZER;

alarm_t * alarm_list = NULL;

time_t current_alarm = 0;

void alarm_insert(alarm_t * alarm)
{
	int status;

	alarm_t ** last, *next;

	last = &alarm_list;

	next = *last;

	while ( next != NULL )
	{
		if ( next->time >= alarm->time )
		{
			alarm->link = next;

			*last = alarm;

			break;
		}

		last = &next->link;

		next = next->link;
	}

	if ( next == NULL )
	{
		*last = alarm;

		alarm->link = NULL;
	}

	printf("[list: ");

	for ( next = alarm_list; next != NULL; next = next->link )
	{
		printf("%d(%d)[\"%s\"] ",next->time,next->time - time(NULL),next->message);
	}

	printf("]\n");

	if ( current_alarm == 0 || alarm->time < current_time )
	{
		currrent_alarm = alarm->time;

		status = pthread_cond_signal(&alarm_cond);

		if ( status != 0 )
		{
			fprintf(stderr,"Error: Signal cond failed at %s in %d: %d\n",__FILE__,__LINE__,status);

			exit(1);
		}
	}
}

void * alarm_thread(void *arg)
{
	alarm_t * alarm;

	struct timespec cond_time;

	time_t now;

	int status, expired;

	status = pthread_mutex_lock(&alarm_mutex);

	if ( status != 0 )
	{
		fprintf(stderr,"Error: Wait on condition failed in %s in %d: %d\n",__FILE__,__LINE__,status);

		exit(1);
	}

	alarm = alarm_list;

	alarm_list = alarm->link;

	now = time(NULL);

	expired = 0;

	if ( alarm->time > now )
	{
		printf("[waiting: %d(%d)\"%s\"]\n",alarm->time,alarm->time - time(NULL),alarm->message);

		cond_time.tv_sec = alarm->time;

		cond_time.tv_nsec = 0;

		current_alarm = alarm->time;

		while ( current_alarm == alarm->time )
		{
			status = pthread_cond_timedwait(&alarm_cond,&alarm_mutex,&cond_time);

			if ( status != 0 )
			{
				fprintf(stderr,"Error: Wait on condition failed in %s in %d: %d\n",__FILE__,__LINE__,status);
				exit(1);
			}
		}

		alarm = alarm_list;

		alarm_list = alarm->link;

		now = time(NULL);

		expired = 0;

		if ( alarm->time > now )
		{
			printf("[waiting: %d(%d)\"%s\"
		}

	}
}
