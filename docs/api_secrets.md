# Protecting API Secrets

To protect API secrets, never store API secrets in any git directory.

Even if a git repository is private, it is still a very bad habit

to do this.

Instead, consider the following:

1. Store the secret in a file **outside** of the git directory

and have your program read it from the external location.

2. Use a `env` file. Just as with (1.), this will store the variable

as a terminal shell environment variable and will be stored in the

computer's local machine directly by the system process that needs them.

Therefore, once a system process that uses those environment variables

ends, the environment variables themselves are also wiped from RAM.

Care must be taken to store the `env` file in the first place.

3. Store file containing API secrets in encrypted form. This is arguably

the best way to manage the API secrets.

In the beta product, a C program will read the API secrets from standard

input and store it, in encrypted form, in a file. This file can then

be saved in a password manager for safekeeping.

To open the file, the beta program itself may ask for a password to decrypt

the file. The file will be stored in the computer's RAM, write and read

protected by special RAM access functions provided by LibSodium.
