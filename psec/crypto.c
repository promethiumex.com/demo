#ifndef __PSEC_H__
#define __PSEC_H__
#include "../headers/psec.h"
#endif

void enc_string_to_file(const unsigned char * filename,size_t n)
{
	
	if ( sodium_init() < 0 )
	{
		fprintf(stderr,"Error: Failed to initialize LibSodium in enc_string_to_file().\nAborting.\n");

		exit(0);
	}

	unsigned char c = 0;

	size_t i = 0;

	unsigned char * s = (unsigned char*)sodium_malloc(n*sizeof(unsigned char)), * s_p = s;

	if ( s == NULL )
	{
		fprintf(stderr,"Error: Failed to allocate string s in enc_string_to_file().\nAborting.\n");
		sodium_memzero(s,n);

		sodium_free(s);

		exit(0);
	}

	memset_s(s,sizeof(s),0x00,sizeof(s));

	while ( ( i < n ) && ( ( c = fgetc(stdin) ) != 0xff) )
	{
		*s_p++ = c;

		i++;
	}

	if ( c != 0xff )
	{
		fprintf(stderr,"Error: Failed to read entire stdin in enc_string_to_file().\nAborting.\n");

		exit(0);
	}


	sodium_mprotect_noaccess(s);

	unsigned char * nonce = (unsigned char*)sodium_malloc((crypto_aead_xchacha20poly1305_ietf_NPUBBYTES+1)*sizeof(unsigned char));
	
	if ( nonce == NULL )
	{
		fprintf(stderr,"Error: Failed to allocate string nonce in enc_string_to_file().\nAborting.\n");
		sodium_memzero(s,n);

		sodium_memzero(nonce,(crypto_aead_xchacha20poly1305_ietf_NPUBBYTES+1)*sizeof(unsigned char));
		sodium_free(s);

		sodium_free(nonce);

		exit(0);
	}

	memset_s(nonce,(crypto_aead_xchacha20poly1305_ietf_NPUBBYTES+1)*sizeof(unsigned char),0x00,crypto_aead_xchacha20poly1305_ietf_NPUBBYTES+1);

	randombytes_buf(nonce,crypto_aead_xchacha20poly1305_ietf_NPUBBYTES*sizeof(unsigned char));

	sodium_mprotect_noaccess(nonce);

	unsigned char * key = (unsigned char*)sodium_malloc((crypto_aead_xchacha20poly1305_ietf_KEYBYTES+1)*sizeof(unsigned char));
	
	if ( key == NULL )
	{
		fprintf(stderr,"Error: Failed to allocate string key in enc_string_to_file().\nAborting.\n");
		sodium_memzero(s,n);

		sodium_memzero(nonce,(crypto_aead_xchacha20poly1305_ietf_NPUBBYTES+1)*sizeof(unsigned char));
		sodium_memzero(key,(crypto_aead_xchacha20poly1305_ietf_KEYBYTES+1)*sizeof(unsigned char));
		sodium_free(key);

		sodium_free(s);

		sodium_free(nonce);

		exit(0);
	}

	memset_s(key,(crypto_aead_xchacha20poly1305_ietf_KEYBYTES+1)*sizeof(unsigned char),0x00,(crypto_aead_xchacha20poly1305_ietf_KEYBYTES+1)*sizeof(unsigned char));

	// Securely read the user's raw password first
	
	unsigned char * pwd = (unsigned char*)sodium_malloc(1025*sizeof(unsigned char));

	if ( pwd == NULL )
	{
		fprintf(stderr,"Error: Failed to allocate pwd in enc_string_to_file().\nAborting.\n");
		sodium_memzero(s,n);

		sodium_memzero(nonce,(crypto_aead_xchacha20poly1305_ietf_NPUBBYTES+1)*sizeof(unsigned char));
		sodium_memzero(key,(crypto_aead_xchacha20poly1305_ietf_KEYBYTES+1)*sizeof(unsigned char));
		sodium_memzero(pwd,1025*sizeof(unsigned char));
		sodium_free(pwd);

		sodium_free(key);

		sodium_free(s);

		sodium_free(nonce);

		exit(0);
	}

	memset_s(pwd,1025*sizeof(unsigned char),0x00,1025*sizeof(unsigned char));

	unsigned char * repwd = (unsigned char*)sodium_malloc(1025*sizeof(unsigned char*));

	if ( repwd == NULL )
	{
		fprintf(stderr,"Error: Failed to allocate repwd in enc_string_to_file().\nAborting.\n");
		sodium_memzero(s,n);

		sodium_memzero(nonce,(crypto_aead_xchacha20poly1305_ietf_NPUBBYTES+1)*sizeof(unsigned char));
		sodium_memzero(key,(crypto_aead_xchacha20poly1305_ietf_KEYBYTES+1)*sizeof(unsigned char));
		sodium_memzero(pwd,1025*sizeof(unsigned char));
		sodium_memzero(repwd,1025*sizeof(unsigned char*));

		sodium_free(repwd);

		sodium_free(pwd);

		sodium_free(key);

		sodium_free(s);

		sodium_free(nonce);

		exit(0);

	}

	memset_s(repwd,1025*sizeof(unsigned char),0x00,1025*sizeof(unsigned char));

	rewind(stdin);

	fflush(stdin);

	if ( get_pass(pwd,repwd,1024*sizeof(unsigned char),stdin) == 0 )
	{
		fprintf(stderr,"Error: Failed to retrieve password in enc_string_to_file().\nAborting\n");

		exit(0);
	}

	sodium_mprotect_noaccess(pwd);
	
	sodium_mprotect_noaccess(repwd);
	
	// hash the symmetric key with Argon2ID second
	
	unsigned char salt[crypto_pwhash_SALTBYTES];

	memset_s(salt,crypto_pwhash_SALTBYTES,0x00,crypto_pwhash_SALTBYTES);

	sodium_mprotect_readwrite(pwd);

	if ( 
			crypto_pwhash	(
					
					key
					
					,
					
					crypto_aead_xchacha20poly1305_ietf_KEYBYTES*sizeof(unsigned char)
					
					
					,
					
					pwd
					
					,
					
					strnlen_s(pwd,1024*sizeof(unsigned char))
						
					,
					
					salt	
					
					,
					
					crypto_pwhash_OPSLIMIT_INTERACTIVE
					
					,
					
					crypto_pwhash_MEMLIMIT_INTERACTIVE
					
					,
					
					crypto_pwhash_ALG_ARGON2ID13
						
					) 
					
					!= 
					
					0 
					
	)
	{
		fprintf(stderr,"Error: Failed to hash symmetric key in enc_string_to_file().\nAborting.\n");
		sodium_memzero(s,n);

		sodium_memzero(nonce,(crypto_aead_xchacha20poly1305_ietf_NPUBBYTES+1)*sizeof(unsigned char));
		sodium_memzero(key,(crypto_aead_xchacha20poly1305_ietf_KEYBYTES+1)*sizeof(unsigned char));
		sodium_memzero(pwd,1025*sizeof(unsigned char));
		sodium_memzero(repwd,1025*sizeof(unsigned char*));

		sodium_free(repwd);

		sodium_free(pwd);

		sodium_free(key);

		sodium_free(s);

		sodium_free(nonce);

		exit(0);
	}

	

	sodium_mprotect_noaccess(pwd);	
	
	unsigned char ciphertext[(1025 + crypto_aead_xchacha20poly1305_ietf_ABYTES)*sizeof(unsigned char)];

	memset_s(ciphertext,(1025 + crypto_aead_xchacha20poly1305_ietf_ABYTES)*sizeof(unsigned char),0x00,(1025 + crypto_aead_xchacha20poly1305_ietf_ABYTES)*sizeof(unsigned char));

	unsigned long long int cipherlen = 0;

	sodium_mprotect_readwrite(s);

	sodium_mprotect_readwrite(nonce);

	crypto_aead_xchacha20poly1305_ietf_encrypt(ciphertext,&cipherlen,s,strnlen_s(s,n*sizeof(unsigned char)),NULL,0,NULL,nonce,key);

	sodium_mprotect_noaccess(nonce);

	sodium_mprotect_noaccess(s);

	sodium_mprotect_noaccess(key);	

	// Base64 Encode the nonce
	
	unsigned long long int base64_noncelen = (unsigned long long int)(4.0*ceil(crypto_aead_xchacha20poly1305_ietf_NPUBBYTES*sizeof(unsigned char)/3.0));

	unsigned char * base64_nonce = (unsigned char*)sodium_malloc(base64_noncelen+1);

	if ( base64_nonce == NULL )
	{
		fprintf(stderr,"Error: Failed to allocate base64_nonce in enc_string_to_file().\nAborting.\n");
		sodium_memzero(base64_nonce,base64_noncelen+1);
		sodium_free(base64_nonce);

		sodium_memzero(s,n);

		sodium_memzero(nonce,(crypto_aead_xchacha20poly1305_ietf_NPUBBYTES+1)*sizeof(unsigned char));
		sodium_memzero(key,(crypto_aead_xchacha20poly1305_ietf_KEYBYTES+1)*sizeof(unsigned char));
		sodium_memzero(pwd,1025*sizeof(unsigned char));
		sodium_memzero(repwd,1025*sizeof(unsigned char*));

		sodium_free(repwd);

		sodium_free(pwd);

		sodium_free(key);

		sodium_free(s);

		sodium_free(nonce);

		exit(0);
	}

	memset_s(base64_nonce,base64_noncelen+1,0x00,base64_noncelen+1);

	sodium_mprotect_readwrite(nonce);

	base64_encode(nonce,crypto_aead_xchacha20poly1305_ietf_NPUBBYTES*sizeof(unsigned char),base64_nonce);

	sodium_mprotect_noaccess(base64_nonce);

	// Base64 Encode the ciphertext
	
	unsigned long long int base64_cipherlen = (unsigned long long int)(4.0 * ceil(cipherlen/3.0));
	
	unsigned char * base64_ciphertext = (unsigned char*)sodium_malloc(base64_cipherlen+1);

	if ( base64_ciphertext == NULL )
	{
		fprintf(stderr,"Error: Failed to allocate for base64_ciphertext in enc_string_to_file.\nAborting.\n");

		sodium_memzero(base64_ciphertext,base64_cipherlen+1);
		
		sodium_memzero(base64_nonce,base64_noncelen+1);
		sodium_free(base64_nonce);

		sodium_memzero(s,n);

		sodium_memzero(nonce,(crypto_aead_xchacha20poly1305_ietf_NPUBBYTES+1)*sizeof(unsigned char));
		sodium_memzero(key,(crypto_aead_xchacha20poly1305_ietf_KEYBYTES+1)*sizeof(unsigned char));
		sodium_memzero(pwd,1025*sizeof(unsigned char));
		sodium_memzero(repwd,1025*sizeof(unsigned char*));

		sodium_free(repwd);

		sodium_free(pwd);

		sodium_free(key);

		sodium_free(s);

		sodium_free(nonce);

		exit(0);
	}

	memset_s(base64_ciphertext,base64_cipherlen+1,0x00,base64_cipherlen+1);

	base64_encode(ciphertext,cipherlen,base64_ciphertext);

	sodium_mprotect_noaccess(base64_ciphertext);

	// Zero-Wipe These first before freeing.

        // Securely write to file.
		
	FILE * out = NULL;

	if ( ( out = fopen(filename,"w+") ) == NULL )
	{
		fprintf(stderr,"Error: Failed to open file in enc_string_to_file() for saving encrypted data\nstream.\nAborting.\n");
		
		sodium_memzero(base64_ciphertext,base64_cipherlen+1);
		
		sodium_memzero(base64_nonce,base64_noncelen+1);
		sodium_free(base64_nonce);

		sodium_memzero(s,n);

		sodium_memzero(nonce,(crypto_aead_xchacha20poly1305_ietf_NPUBBYTES+1)*sizeof(unsigned char));
		sodium_memzero(key,(crypto_aead_xchacha20poly1305_ietf_KEYBYTES+1)*sizeof(unsigned char));
		sodium_memzero(pwd,1025*sizeof(unsigned char));
		sodium_memzero(repwd,1025*sizeof(unsigned char*));

		sodium_free(repwd);

		sodium_free(pwd);

		sodium_free(key);

		sodium_free(s);

		sodium_free(nonce);
		
		exit(0);
	}

	// Write base64_nonce to out file
	
	i = 0;

	sodium_mprotect_readonly(base64_nonce);

	for ( ; i < strnlen_s(base64_nonce,base64_noncelen); i++ )
	{
		fputc(base64_nonce[i],out);
	}

	sodium_mprotect_noaccess(base64_nonce);

	fputc(0x0a,out);

	// Write base64_ciphertext to out file
	
	i = 0;

	sodium_mprotect_readonly(base64_ciphertext);

	for ( ; i < strnlen_s(base64_ciphertext,base64_cipherlen); i++ )
	{
		fputc(base64_ciphertext[i],out);
	}

	sodium_mprotect_noaccess(base64_ciphertext);

	fputc(0x0a,out);

	if ( fclose(out) == 0xff )
	{
		fprintf(stderr,"Error: Failed to close FILE * out in enc_string_to_file().\nAborting.\n");
		sodium_memzero(base64_ciphertext,base64_cipherlen+1);
		
		sodium_memzero(base64_nonce,base64_noncelen+1);
		sodium_free(base64_nonce);

		sodium_memzero(s,n);

		sodium_memzero(nonce,(crypto_aead_xchacha20poly1305_ietf_NPUBBYTES+1)*sizeof(unsigned char));
		sodium_memzero(key,(crypto_aead_xchacha20poly1305_ietf_KEYBYTES+1)*sizeof(unsigned char));
		sodium_memzero(pwd,1025*sizeof(unsigned char));
		sodium_memzero(repwd,1025*sizeof(unsigned char*));

		sodium_free(repwd);

		sodium_free(pwd);

		sodium_free(key);

		sodium_free(s);

		sodium_free(nonce);

		exit(0);
	}

	sodium_mprotect_readwrite(key);
	
	sodium_memzero(key,crypto_aead_xchacha20poly1305_ietf_KEYBYTES*sizeof(unsigned char));

	sodium_free(key);

	sodium_mprotect_readwrite(s);

	sodium_memzero(s,n*sizeof(unsigned char));

	sodium_free(s);

	sodium_mprotect_readwrite(nonce);

	sodium_memzero(nonce,crypto_aead_xchacha20poly1305_ietf_NPUBBYTES*sizeof(unsigned char));

	sodium_free(nonce);

	sodium_mprotect_readwrite(pwd);

	sodium_memzero(pwd,1025*sizeof(unsigned char));

	sodium_free(pwd);
	
	sodium_mprotect_readwrite(repwd);
	
	sodium_memzero(repwd,1025*sizeof(unsigned char));

	sodium_free(repwd);

// 	Stuck here

	sodium_mprotect_readwrite(base64_nonce);

	sodium_memzero(base64_nonce,base64_noncelen);

	sodium_free(base64_nonce);

	sodium_mprotect_readwrite(base64_ciphertext);

	sodium_memzero(base64_ciphertext,base64_cipherlen);

	sodium_free(base64_ciphertext);
	
}

void dec_file_to_string(const unsigned char * filepath,const size_t decrypt_string_size,unsigned char ** out,size_t * outsize)
{
	FILE * in = NULL;

	size_t base64_ciphertextsize = 0;

	if ( ( in = fopen(filepath,"r") ) == NULL )
	{
		fprintf(stderr,"Error: Failed to open filepath in dec_file_to_string().\nAborting\n");

		exit(0);
	}	
	
	unsigned char c = 0;
	
	size_t base64_noncesize = 0;

	while	( 
			( ( c = fgetc(in) ) != 0x0a )

			&&

			( c != 0xff )
		)
	{
		base64_noncesize++;
	}

	if ( c == 0xff )
	{
		fprintf(stderr,"Error: Failed to find first 0x0a byte ending nonce in dec_file_to_string().\nAborting.\n");

		exit(0);
	}	

	rewind(in);

	size_t i = 0;
	
	unsigned char * base64_nonce = (unsigned char*)sodium_malloc(base64_noncesize+1);

	if ( base64_nonce == NULL )
	{
		fprintf(stderr,"Error: Failed to allocate base64_nonce in dec_file_to_string()\n");

		sodium_memzero(base64_nonce,base64_noncesize+1);
		
		sodium_free(base64_nonce);

		exit(0);
	}

	memset_s(base64_nonce,base64_noncesize+1,0x00,base64_noncesize+1);


	while	( 
			( ( c = fgetc(in) ) != 0x0a )

			&&

			( c != 0xff )
		)
	{
		base64_nonce[i] = c;

		i++;
	}

	if ( c == 0xff )
	{
		fprintf(stderr,"Error: Failed to find first 0x0a byte ending nonce in dec_file_to_string().\nAborting.\n");
		sodium_memzero(base64_nonce,base64_noncesize+1);
		
		sodium_free(base64_nonce);


		exit(0);
	}

	long first_enter = ftell(in);

	base64_ciphertextsize = 0;

	while	( 
			( ( c = fgetc(in) ) != 0x0a )

			&&

			( c != 0xff )
		)
	{
		base64_ciphertextsize++;

	}

	if ( c == 0xff )
	{
		fprintf(stderr,"Error: Failed to find second 0x0a byte ending ciphertext in\ndec_file_to_string().\nAborting.\n");

		exit(0);
	}
	
	unsigned char * base64_ciphertext = (unsigned char*)sodium_malloc(base64_ciphertextsize+1);

	if ( base64_ciphertext == NULL )
	{
		fprintf(stderr,"Error: Failed to allocate base64_ciphertext in dec_file_to_string().\n");

		sodium_memzero(base64_ciphertext,base64_ciphertextsize+1);

		sodium_free(base64_ciphertext);
		
		sodium_memzero(base64_nonce,base64_noncesize+1);
		sodium_free(base64_nonce);


		exit(0);
	}

	memset_s(base64_ciphertext,base64_ciphertextsize+1,0x00,base64_ciphertextsize+1);

	// Error above below statement

	if ( fseek(in,first_enter,SEEK_SET) != 0 )
	{
		fprintf(stderr,"Error: Failed to set fseek back to first enter key found.\nAborting.\n");
		
		sodium_memzero(base64_ciphertext,base64_ciphertextsize+1);

		sodium_free(base64_ciphertext);
		
		sodium_memzero(base64_nonce,base64_noncesize+1);
		sodium_free(base64_nonce);

		exit(0);	
	}	

	i = 0;
	
	while	( 
			( ( c = fgetc(in) ) != 0x0a )

			&&

			( c != 0xff )
		)
	{
		base64_ciphertext[i] = c;

		i++;
	}

	if ( c == 0xff )
	{
		fprintf(stderr,"Error: Failed to find second 0x0a byte ending ciphertext in\ndec_file_to_string().\nAborting.\n");
		
		sodium_memzero(base64_ciphertext,base64_ciphertextsize+1);

		sodium_free(base64_ciphertext);
		
		sodium_memzero(base64_nonce,base64_noncesize+1);
		sodium_free(base64_nonce);

		exit(0);
	}

	// Now close the FILE * in
	
	if ( fclose(in) != 0 )
	{
		fprintf(stderr,"Error: Failed to close file.\nAborting.\n");
		
		sodium_memzero(base64_ciphertext,base64_ciphertextsize+1);

		sodium_free(base64_ciphertext);
		
		sodium_memzero(base64_nonce,base64_noncesize+1);
		sodium_free(base64_nonce);

		exit(0);
	}


	// Now decode nonce and ciphertext into their respective arrays
	
	size_t num_pads = 0;

	i = base64_noncesize - 1;

	while ( base64_nonce[i] == '=' )
	{
		num_pads++;

		i--;
	}
	
	size_t noncesize = floor( ( base64_noncesize - num_pads ) * 3.0 / 4.0 );

	num_pads = 0;

	i = base64_ciphertextsize - 1;

	while ( base64_ciphertext[i] == '=' )
	{
		num_pads++;

		i--;
	}

	size_t ciphertextsize = floor( ( base64_ciphertextsize - num_pads ) * 3.0 / 4.0 );

	unsigned char * ciphertext = (unsigned char*)sodium_malloc(ciphertextsize+1);

	if ( ciphertext == NULL )
	{
		fprintf(stderr,"Error: failed to allocate ciphertext in dec_file_to_string()\n");

		sodium_memzero(ciphertext,ciphertextsize+1);
		sodium_free(ciphertext);
		
		sodium_memzero(base64_ciphertext,base64_ciphertextsize+1);

		sodium_free(base64_ciphertext);
		
		sodium_memzero(base64_nonce,base64_noncesize+1);
		sodium_free(base64_nonce);

		exit(0);
	}

	memset_s(ciphertext,ciphertextsize+1,0x00,ciphertextsize+1);

	base64_decode(base64_ciphertext,base64_ciphertextsize,ciphertext);

	i = base64_noncesize - 1;

	num_pads = 0;

	while ( base64_nonce[i] == '=' )
	{
		num_pads++;

		i--;
	}

	noncesize = floor( ( base64_noncesize - num_pads ) * 3.0 / 4.0 );

	unsigned char * nonce = (unsigned char*)sodium_malloc(noncesize+1);

	if ( nonce == NULL )
	{
		fprintf(stderr,"Error: Failed to allocate nonce in dec_file_to_string()\n");

		sodium_memzero(nonce,noncesize+1);

		sodium_free(nonce);
		
		sodium_memzero(ciphertext,ciphertextsize+1);
		sodium_free(ciphertext);
		
		sodium_memzero(base64_ciphertext,base64_ciphertextsize+1);

		sodium_free(base64_ciphertext);
		
		sodium_memzero(base64_nonce,base64_noncesize+1);
		sodium_free(base64_nonce);

		exit(0);
	}

	memset_s(nonce,noncesize+1,0x00,noncesize+1);

	base64_decode(base64_nonce,base64_noncesize,nonce);

	// Now decrypt the entire string
	
	unsigned char * decrypt_string = (unsigned char*)sodium_malloc(decrypt_string_size+1);

	if ( decrypt_string == NULL )
	{
		fprintf(stderr,"Error: Failed to allocate decrypt_string in dec_file_to_string()\n");

		sodium_memzero(decrypt_string,decrypt_string_size+1);
		sodium_free(decrypt_string);
		
		sodium_memzero(nonce,noncesize+1);

		sodium_free(nonce);
		
		sodium_memzero(ciphertext,ciphertextsize+1);
		sodium_free(ciphertext);
		
		sodium_memzero(base64_ciphertext,base64_ciphertextsize+1);

		sodium_free(base64_ciphertext);
		
		sodium_memzero(base64_nonce,base64_noncesize+1);
		sodium_free(base64_nonce);

		exit(0);
	}

	memset_s(decrypt_string,decrypt_string_size+1,0x00,decrypt_string_size+1);

	// get_pass here: MAX_SIZE: 1024
	
	unsigned char * pwd = (unsigned char*)sodium_malloc(1025*sizeof(unsigned char));

	if ( pwd == NULL )
	{
		fprintf(stderr,"Error: failed to allocate pwd in dec_file_to_string()\n");

		sodium_memzero(pwd,1025*sizeof(unsigned char));
		sodium_free(pwd);
		
		sodium_memzero(decrypt_string,decrypt_string_size+1);
		sodium_free(decrypt_string);
		
		sodium_memzero(nonce,noncesize+1);

		sodium_free(nonce);
		
		sodium_memzero(ciphertext,ciphertextsize+1);
		sodium_free(ciphertext);
		
		sodium_memzero(base64_ciphertext,base64_ciphertextsize+1);

		sodium_free(base64_ciphertext);
		
		sodium_memzero(base64_nonce,base64_noncesize+1);
		sodium_free(base64_nonce);

		exit(0);
	}

	memset_s(pwd,1024*sizeof(unsigned char),0x00,1024*sizeof(unsigned char));

	unsigned char * repwd = (unsigned char*)sodium_malloc(1025*sizeof(unsigned char));

	if ( repwd == NULL )
	{
		fprintf(stderr,"Error: Failed to allocate repwd in dec_file_to_string()\n");

		sodium_memzero(repwd,1025*sizeof(unsigned char));
		sodium_free(repwd);
		
		sodium_memzero(pwd,1025*sizeof(unsigned char));
		sodium_free(pwd);
		
		sodium_memzero(decrypt_string,decrypt_string_size+1);
		sodium_free(decrypt_string);
		
		sodium_memzero(nonce,noncesize+1);

		sodium_free(nonce);
		
		sodium_memzero(ciphertext,ciphertextsize+1);
		sodium_free(ciphertext);
		
		sodium_memzero(base64_ciphertext,base64_ciphertextsize+1);

		sodium_free(base64_ciphertext);
		
		sodium_memzero(base64_nonce,base64_noncesize+1);
		sodium_free(base64_nonce);

		exit(0);
	}

	memset_s(repwd,1025*sizeof(unsigned char),0x00,1025*sizeof(unsigned char));
	
	rewind(stdin);

	fflush(stdin);

	if ( get_pass(pwd,repwd,1025*sizeof(unsigned char),stdin) == 0 )
	{
		fprintf(stderr,"Error: Failed to retrieve password in dec_file_to_string().\nAborting\n");
		
		sodium_memzero(repwd,1025*sizeof(unsigned char));
		sodium_free(repwd);
		
		sodium_memzero(pwd,1025*sizeof(unsigned char));
		sodium_free(pwd);
		
		sodium_memzero(decrypt_string,decrypt_string_size+1);
		sodium_free(decrypt_string);
		
		sodium_memzero(nonce,noncesize+1);

		sodium_free(nonce);
		
		sodium_memzero(ciphertext,ciphertextsize+1);
		sodium_free(ciphertext);
		
		sodium_memzero(base64_ciphertext,base64_ciphertextsize+1);

		sodium_free(base64_ciphertext);
		
		sodium_memzero(base64_nonce,base64_noncesize+1);
		sodium_free(base64_nonce);

		exit(0);
	}

	sodium_mprotect_noaccess(repwd);

	(*out) = sodium_malloc(base64_ciphertextsize+1);

	if ( (*out) == NULL )
	{
		fprintf(stderr,"Error: failed to allocate (*out) in dec_file_to_string()\n");
		
		sodium_memzero(repwd,1025*sizeof(unsigned char));
		sodium_free(repwd);
		
		sodium_memzero(pwd,1025*sizeof(unsigned char));
		sodium_free(pwd);
		
		sodium_memzero(decrypt_string,decrypt_string_size+1);
		sodium_free(decrypt_string);
		
		sodium_memzero(nonce,noncesize+1);

		sodium_free(nonce);
		
		sodium_memzero(ciphertext,ciphertextsize+1);
		sodium_free(ciphertext);
		
		sodium_memzero(base64_ciphertext,base64_ciphertextsize+1);

		sodium_free(base64_ciphertext);
		
		sodium_memzero(base64_nonce,base64_noncesize+1);
		sodium_free(base64_nonce);

		exit(0);
	}

	memset_s((*out),base64_ciphertextsize+1,0x00,base64_ciphertextsize+1);

	// Forgot to hash pwd
	
	unsigned char * key = (unsigned char*)sodium_malloc((crypto_aead_xchacha20poly1305_ietf_KEYBYTES+1)*sizeof(unsigned char));
	
	if ( key == NULL )
	{
		fprintf(stderr,"Error: Failed to allocate string key in enc_string_to_file().\nAborting.\n");
		sodium_memzero(key,(crypto_aead_xchacha20poly1305_ietf_KEYBYTES+1)*sizeof(unsigned char));

		sodium_free(key);

		sodium_memzero(repwd,1025*sizeof(unsigned char));
		sodium_free(repwd);
		
		sodium_memzero(pwd,1025*sizeof(unsigned char));
		sodium_free(pwd);
		
		sodium_memzero(decrypt_string,decrypt_string_size+1);
		sodium_free(decrypt_string);
		
		sodium_memzero(nonce,noncesize+1);

		sodium_free(nonce);
		
		sodium_memzero(ciphertext,ciphertextsize+1);
		sodium_free(ciphertext);
		
		sodium_memzero(base64_ciphertext,base64_ciphertextsize+1);

		sodium_free(base64_ciphertext);
		
		sodium_memzero(base64_nonce,base64_noncesize+1);
		sodium_free(base64_nonce);

		exit(0);
	}

	memset_s(key,(crypto_aead_xchacha20poly1305_ietf_KEYBYTES+1)*sizeof(unsigned char),0x00,(crypto_aead_xchacha20poly1305_ietf_KEYBYTES+1)*sizeof(unsigned char));

	// hash the symmetric key with Argon2ID second
	
	unsigned char salt[crypto_pwhash_SALTBYTES];

	memset_s(salt,crypto_pwhash_SALTBYTES,0x00,crypto_pwhash_SALTBYTES);
	
	if ( 
			crypto_pwhash	(
					
					key
					
					,
					
					crypto_aead_xchacha20poly1305_ietf_KEYBYTES*sizeof(unsigned char)
					
					
					,
					
					pwd
					
					,
					
					strnlen_s(pwd,1024*sizeof(unsigned char))
						
					,
					
					salt	
					
					,
					
					crypto_pwhash_OPSLIMIT_INTERACTIVE
					
					,
					
					crypto_pwhash_MEMLIMIT_INTERACTIVE
					
					,
					
					crypto_pwhash_ALG_ARGON2ID13
						
					) 
					
					!= 
					
					0 
					
	)
	{
		fprintf(stderr,"Error: Failed to hash symmetric key in enc_string_to_file().\nAborting.\n");
		
		sodium_memzero(key,(crypto_aead_xchacha20poly1305_ietf_KEYBYTES+1)*sizeof(unsigned char));

		sodium_free(key);

		sodium_memzero(repwd,1025*sizeof(unsigned char));
		sodium_free(repwd);
		
		sodium_memzero(pwd,1025*sizeof(unsigned char));
		sodium_free(pwd);
		
		sodium_memzero(decrypt_string,decrypt_string_size+1);
		sodium_free(decrypt_string);
		
		sodium_memzero(nonce,noncesize+1);

		sodium_free(nonce);
		
		sodium_memzero(ciphertext,ciphertextsize+1);
		sodium_free(ciphertext);
		
		sodium_memzero(base64_ciphertext,base64_ciphertextsize+1);

		sodium_free(base64_ciphertext);
		
		sodium_memzero(base64_nonce,base64_noncesize+1);
		sodium_free(base64_nonce);
		exit(0);
	}

	if (
		crypto_aead_xchacha20poly1305_ietf_decrypt((*out),(*outsize),NULL,ciphertext,ciphertextsize,NULL,0,nonce,key) != 0

	   )
	{
		fprintf(stderr,"Error: Failed to decrypt string in dec_string_to_file()\n.Aborting.\n");
		
		sodium_memzero(key,(crypto_aead_xchacha20poly1305_ietf_KEYBYTES+1)*sizeof(unsigned char));

		sodium_free(key);

		sodium_memzero(repwd,1025*sizeof(unsigned char));
		sodium_free(repwd);
		
		sodium_memzero(pwd,1025*sizeof(unsigned char));
		sodium_free(pwd);
		
		sodium_memzero(decrypt_string,decrypt_string_size+1);
		sodium_free(decrypt_string);
		
		sodium_memzero(nonce,noncesize+1);

		sodium_free(nonce);
		
		sodium_memzero(ciphertext,ciphertextsize+1);
		sodium_free(ciphertext);
		
		sodium_memzero(base64_ciphertext,base64_ciphertextsize+1);

		sodium_free(base64_ciphertext);
		
		sodium_memzero(base64_nonce,base64_noncesize+1);
		sodium_free(base64_nonce);

		exit(0);
	}

	unsigned char * outp = (*out);

	i = 0;

	while (
			( i < ( base64_ciphertextsize + 1 ) )

			&&

		        ( i < decrypt_string_size )

			&&

			( *outp != 0xdb )

	      )
	{
		outp++;

		i++;
	}

	while ( 
			( i < base64_ciphertextsize+1 )

			&&

		        ( i < decrypt_string_size )

			&&

			( *outp == 0xdb )
	      )
	{
		*outp++ = 0x00;

		i++;
	}

	sodium_mprotect_noaccess((*out));

	sodium_mprotect_readwrite(repwd);

	sodium_memzero(repwd,1025*sizeof(unsigned char));
	
	sodium_free(repwd);

	sodium_memzero(pwd,1025*sizeof(unsigned char));
	
	sodium_free(pwd);

	sodium_memzero(decrypt_string,decrypt_string_size+1);

	sodium_free(decrypt_string);

	sodium_memzero(nonce,noncesize+1);

	sodium_free(nonce);
	
	sodium_memzero(ciphertext,ciphertextsize+1);

	sodium_free(ciphertext);	

	sodium_memzero(base64_nonce,base64_noncesize+1);

	sodium_free(base64_nonce);

	sodium_memzero(base64_ciphertext,base64_ciphertextsize+1);

	sodium_free(base64_ciphertext);
}
