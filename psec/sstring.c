#ifndef __PSEC_H__
#define __PSEC_H__
#include "../headers/psec.h"
#endif

size_t memset_s(unsigned char * dest,size_t dest_size,unsigned char ch,size_t count)
{

	if ( dest == 0x00 ) return 0;

	if ( count > dest_size ) return 0;

	size_t i = 0;

	for ( ;i < count; i++ )
	{
		dest[i] = ch;
	}

	return i;

}

size_t strnlen_s(unsigned char *s,const size_t n)
{
	if ( s == 0x00 ) return 0x00;

	unsigned char * s_p = (unsigned char*)s;

	size_t i = 0;

	for ( ; (i < n) && (*s_p != 0x00) ; i++ )
	{
		s_p++;	
	}

	return i; 
}

size_t strncat_s(unsigned char * dst,const size_t dst_size,const unsigned char * src,size_t src_size,size_t n)
{

	if ( ( dst == 0x00 ) || ( src == 0x00 ) )
	{
		fprintf(stderr,"Error: Either dst or src array are NULL\n");

		return 0;
	}

	if ( dst_size == 0 )
	{
		fprintf(stderr,"Error: dst_size == 0\n");

		return 0;
	}

	size_t i = 0;

	while ( i < dst_size )
	{
		if ( dst[i] == 0x00 ) break;

		i++;
	}

	if ( i == dst_size ) 
	{
		fprintf(stderr,"Error: Failed to set a NULL byte in destination array or mistaken capacity of array for string length. Aborting\n");

		return 0;
	}

	if ( ( n <= src_size ) && ( n > (dst_size-i-1) ) )
	{
		fprintf(stderr,"Error: Destination array too short to allow for proper concatentation\n\nReason: Number of characters to be concatenated exceeds number of bytes remaining after first null byte in destination array\n");

		return 0;
	}

	if ( ( src_size <  n ) && ( src_size > (dst_size-i-1) ) )
	{
		fprintf(stderr,"Error: Desination array too short to allow for proper concatentation\n\nReason: Size of source array exceeds number of bytes remaining after first null byte in destination array\n");

		return 0;
	}

	if ( ( &dst[0] >= &src[0] ) && ( &dst[0] <= &src[src_size-1]) )
	{
		fprintf(stderr,"Error: Destination and source arrays overlap in memory\n");

		return 0;
	}
	
	if ( ( &src[0] >= &dst[0] ) && ( &src[0] <= &dst[dst_size-1]) )
	{
		fprintf(stderr,"Error: Destination and source arrays overlap in memory\n");

		return 0;
	}


	for ( i = 0; dst[i] != 0x00 ; i++ )
		;

	for ( size_t c = 0; (i < dst_size) && (c < n) ; c++, i++ )
	{
		dst[i] = src[c];
	}

	if ( i < dst_size )
	{
		dst[i] = 0x00;
	}

	return i;
			
}

unsigned char * strnstr_s(unsigned char * str,const size_t str_size,unsigned char * substr)
{
	if ( (str == NULL) || ( substr == NULL) )
	{
		return NULL;
	}

	size_t i = 0, j = 0;	

	for ( ; i < str_size; i++ )
	{
		if ( str[i] == 0x00 )
		{
			break;	
		}

	}

	if ( i == str_size )
	{
		fprintf(stderr,"Error: source string str in strnstr_s() is NOT NULL-terminated\n. Returning NULL\n");
		return NULL;
	}

	unsigned char * s = 0x00;

	if ( ( s = (unsigned char*)calloc(str_size+1,sizeof(unsigned char)) ) == NULL )
	{
		fprintf(stderr,"Error: Failed to allocate to string s in strnstr_s().\nAborting.\n");

		return NULL;
	}


	for ( i = 0, j = str_size - 1; i <= j; i++, j-- )
	{
		s[i] = str[i];

		s[j] = str[j];

	}

	unsigned char * raw_result = search(s,substr);
	
	unsigned char * result = (unsigned char*)&str[raw_result - &s[0]];

	free(s);

	return result;

}

