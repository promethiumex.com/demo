#ifndef __PSEC_H__
#define __PSEC_H__
#include "../headers/psec.h"
#endif

int get_pass(unsigned char * pwd,unsigned char * repwd,const size_t MAX_SIZE, FILE * stream)	{
	
	unsigned char * p = pwd;

	unsigned char * r = repwd;

	struct termios old = {0},recent = {0};

	size_t n = 0;

	//Turn echoing into stdout off and exit if my_getpass cannot.
	
	if (tcgetattr(fileno(stream),&old) != 0)	{
		
		fprintf(stderr,"Error: Failed to turn off echoing with tcgetaddr().\n");

		return 0;
	}

	recent = old;

	recent.c_lflag &= ~ECHO;

	if (tcsetattr(fileno(stream),TCSAFLUSH,&recent) != 0)	{
		
		fprintf(stderr,"Error: Failed to turn off echoing with tcsetaddr().\n");

		return 0;
	}

	//Now read the password form stdin
	
	printf("Enter Password:");

	while ( ( (*p = getchar()) != 0xa ) && ( n < MAX_SIZE ) )	{
		
		p++;

		n++;
	}

	putchar(0xa);
	
	n = 0;
	
	printf("Reenter Password:");

	while ( ( ( *r = getchar()) != 0xa ) && ( n < MAX_SIZE) )	{
		
		r++;

		n++;

	}

	putchar(0xa);

	n = 0;

	//Restoring terminal to echoing:
	
	(void) tcsetattr(fileno(stream),TCSAFLUSH,&old);

	if ( strncmp(pwd,repwd,MAX_SIZE) != 0 )	{
		
		fprintf(stderr,"Error:Passwords do not match. Try again.\n");
		
		return 0;
	}

	return 1;
	
}

