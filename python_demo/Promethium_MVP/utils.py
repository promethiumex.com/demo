"""
Product: PromethiumEX MVP
Location: Los Angeles, California
Owned by PromethiumEX LLC
"""
from datetime import datetime
from datetime import timedelta
import base64
import os
import datetime
import time
from werkzeug.wrappers import response
import requests
import json
import jsonify
import binascii
import pysodium
import sqlite3
import _thread
import time
import signal
from getpass import getpass
from dotenv import load_dotenv
from pathlib import Path

dotenv_path = Path('/tmp/.env_enc')

load_dotenv(dotenv_path=dotenv_path)

'''
Decrypt PLAID_SECRET_DEVELOPMENT below
'''

PLAID_CLIENT_ID = os.getenv('PLAID_CLIENT_ID')


'''
Place code to decrypt PLAID_SECRET_DEVELOPMENT key here
'''

salt = binascii.unhexlify(b'00000000000000000000000000000000')

print("Enter the password to decrypt API Secret:")

raw_passwd = getpass()

key = pysodium.crypto_pwhash(pysodium.crypto_auth_KEYBYTES,raw_passwd,salt,pysodium.crypto_pwhash_argon2id_OPSLIMIT_INTERACTIVE,pysodium.crypto_pwhash_argon2id_MEMLIMIT_INTERACTIVE,pysodium.crypto_pwhash_ALG_ARGON2ID13)

ad = binascii.unhexlify(b'00000000000000000000000000000000')

nonce = binascii.unhexlify(b"000000000000000000000000000000000000000000000000")

raw_msg = os.getenv('PLAID_SECRET_DEVELOPMENT')

secret_raw = bytes.fromhex(raw_msg)

'''
1. Convert secret_hexstring from str to hex-encoded 'bytes' object

2. Convert this 'bytes' object back to a raw 'bytes' array
'''

z = pysodium.crypto_aead_xchacha20poly1305_ietf_decrypt(secret_raw,ad,nonce,key)

'''
1. In variable z, the secret development key is a 'bytes' array. You must convert it

to string form.
'''
zstr = z.decode("utf-8")

PLAID_SECRET_DEVELOPMENT = zstr

PLAID_ENV_DEVELOPMENT = os.getenv('PLAID_ENV_DEVELOPMENT')
PLAID_PRODUCTS = os.getenv('PLAID_PRODUCTS', 'transactions').split(',')
PLAID_COUNTRY_CODES = os.getenv('PLAID_COUNTRY_CODES')
INSTITUTION_ID = os.getenv('INSTITUTION_ID')
CLIENT_NAME = os.getenv('CLIENT_NAME')

SEARCH_INSTITUTIONS_URL = os.getenv('SEARCH_INSTITUTIONS_URL')
GET_LINK_TOKEN_URL = os.getenv('GET_LINK_TOKEN_URL')
GET_PUBLIC_TOKEN_URL = os.getenv('GET_PUBLIC_TOKEN_URL')
GET_ACCESS_TOKEN_URL = os.getenv('GET_ACCESS_TOKEN_URL')
GET_ACCOUNT_SUMMARY_URL = os.getenv('GET_ACCOUNT_SUMMARY_URL')
GET_BANK_BALANCE_URL = os.getenv('GET_BANK_BALANCE_URL')

PLAID_REDIRECT_URI = 'http://localhost:3000/'

accessToken = None
payment_id = None
item_id = None

# For all entries older than two hours:

# First, invalidate access token

# Second, delete entry

def flush_development_db(db_file):
   db = sqlite3.connect(db_file)

   cur = db.cursor()

   sql_statement = "DELETE FROM users WHERE (?) > deadline"

   cur.execute(sql_statement,(time.time(),))

   db.commit()

   db.close()

def validBankAccount(bankAccount):
    length = len(bankAccount)

    if bankAccount == "":
        return False

    for i in range(length):
        if bankAccount[i].isdigit() == False:
            return False
    return True

def searchInstutions(bankQuery):
    headers = {"Content-Type": "application/json"}
 
    data = {
        "client_id": PLAID_CLIENT_ID,
        "secret": PLAID_SECRET_DEVELOPMENT,
        "query": bankQuery,
        "products": ['auth', 'balance'],
        "country_codes": ['US']
    }

    response = requests.post(SEARCH_INSTITUTIONS_URL, headers=headers, json=data)

    return response.json()

# Make createLinkToken() here:

'''
getLinkToken() already creates a Link Token for you

Must set a Cookie here and give to user since user will use

information it receives to later cause their access token to

be stored
'''

def getLinkToken():

    headers = {"Content-Type": "application/json"}
 
    data = {
        "client_id": PLAID_CLIENT_ID,
        "secret": PLAID_SECRET_DEVELOPMENT,
        "client_name": CLIENT_NAME,
        "country_codes": ["US"],
        "language": "en",
        "products": ["auth"],
        "user": 
            {
                "client_user_id": PLAID_CLIENT_ID
            },
        }

    response = requests.post(GET_LINK_TOKEN_URL, headers=headers,json=data)

    link_token_resp = response.json()

    return link_token_resp

'''
This function is not useful for Development--only for Sandbox.
'''
def getPublicToken():
    headers = {"Content-Type": "application/json"}
 
    data = {
        "client_id": PLAID_CLIENT_ID,
        "secret": PLAID_SECRET_DEVELOPMENT,
        "institution_id": INSTITUTION_ID,
        "initial_products": ["auth"]
        }

    response = requests.post(GET_PUBLIC_TOKEN_URL, headers=headers, json=data)

    return response.json()

def generateAccessToken():
    global accessToken

    print("Debug: ", getPublicToken() )

    publicToken = getPublicToken()

    headers = {"Content-Type": "application/json"}
 
    data = {
        "client_id": PLAID_CLIENT_ID,
        "secret": PLAID_SECRET_DEVELOPMENT,
        "public_token": publicToken['public_token'],
    }

    response = requests.post(GET_ACCESS_TOKEN_URL, headers=headers, json=data)

    accessToken = response.json()["access_token"]

def getAccountSummary():
    headers = {"Content-Type": "application/json"}

    data = {
        "client_id": PLAID_CLIENT_ID,
        "secret": PLAID_SECRET_DEVELOPMENT,
        "access_token": accessToken
    }

    response = requests.post(GET_ACCOUNT_SUMMARY_URL, headers = headers, json = data)

    return response.json()

def getBankBalance():
    headers = {"Content-Type": "application/json"}

    data = {
        "client_id": PLAID_CLIENT_ID,
        "secret": PLAID_SECRET_DEVELOPMENT,
        "access_token": accessToken
    }

    response = requests.post(GET_BANK_BALANCE_URL, headers = headers, json = data)

    return response.json()["accounts"][0]["balances"]["available"]

# Can expect either 'str' argument or 'bytes' argument

# Returns 'str' argument

def enc_development_data(arg):
    
    salt = binascii.unhexlify(b'00000000000000000000000000000000')

    key = pysodium.crypto_pwhash(pysodium.crypto_auth_KEYBYTES,raw_passwd,salt,pysodium.crypto_pwhash_argon2id_OPSLIMIT_INTERACTIVE,pysodium.crypto_pwhash_argon2id_MEMLIMIT_INTERACTIVE,pysodium.crypto_pwhash_ALG_ARGON2ID13)

    ad = binascii.unhexlify(b'00000000000000000000000000000000')

    nonce = binascii.unhexlify(b"000000000000000000000000000000000000000000000000")

    if type(bytes('string here','utf-8')) != type(arg):

        y = pysodium.crypto_aead_xchacha20poly1305_ietf_encrypt(bytes(arg,'utf-8'),ad,nonce,key)

    else:
        
        y = pysodium.crypto_aead_xchacha20poly1305_ietf_encrypt(arg,ad,nonce,key)

# Converts encrypted output to a hex 'bytes' array and finally to an 'str'

    hexified = binascii.hexlify(y).decode()

    return hexified


# Expects hexadecimal string argument

def dec_development_data(parameter):
    
   salt = binascii.unhexlify(b'00000000000000000000000000000000')

   key = pysodium.crypto_pwhash(pysodium.crypto_auth_KEYBYTES,raw_passwd,salt,pysodium.crypto_pwhash_argon2id_OPSLIMIT_INTERACTIVE,pysodium.crypto_pwhash_argon2id_MEMLIMIT_INTERACTIVE,pysodium.crypto_pwhash_ALG_ARGON2ID13)

   ad = binascii.unhexlify(b'00000000000000000000000000000000')

   nonce = binascii.unhexlify(b"000000000000000000000000000000000000000000000000")

   secret_raw = bytes.fromhex(parameter)
   
   z = pysodium.crypto_aead_xchacha20poly1305_ietf_decrypt(secret_raw,ad,nonce,key)

   zstr = z.decode("utf-8")

   return zstr

def fetch_development_access_token(public_token):
   
#    cookie_random = pysodium.randombytes(32)

# Convert session_id to Base64

#    cookie_random = base64.b64encode(session_id.encode("utf-8"))

#    cookie_random = str(session_id,"utf-8")

#    cookie_body = "__Host-token=" + cookie_random + " ; path=/get-link-token ; Secure ; SameSite=Lax ; HttpOnly"

   headers = {"Content-Type": "application/json"}
    
   data = {
        "client_id": PLAID_CLIENT_ID,
        "secret": PLAID_SECRET_DEVELOPMENT,
        "public_token": public_token
   }

   response = requests.post(GET_ACCESS_TOKEN_URL,headers=headers,json=data)

   resp = response.json()

#    session_id = pysodium.randombytes(32)

# Convert session_id to Base64

#    session_id = base64.b64encode(session_id.encode("utf-8"))

#    session_id = str(session_id,"utf-8")

   access_token_enc = enc_development_data(resp['access_token'])

   return access_token_enc

# Both parameters must be of type 'str' and must be hexadecimal strings

# Both encrypted hexstrings are converted to their base64-encoded secret string forms

# verify_session must accept entire request_body_json to test if user actually

# already has a valid session

def verify_session(json_resp,db_file):

   cookie_body_resp = json_resp['Cookie']

   session_id_resp = cookie_body_resp[len('__Host-token='):cookie_body_resp.index(' ')]

   anti_csrf_token = json_resp['X-CSRF-TOKEN']
   
   session_id_hmac = hmac_message(raw_passwd,session_id_resp)

   anti_csrf_token_hmac = hmac_message(raw_passwd,anti_csrf_token)

   db = sqlite3.connect(db_file)

   cursor = db.cursor()

   sql_data = "SELECT session_id_hmac from users WHERE session_id_hmac=\'" + session_id_hmac + "\'" 

   cursor.execute(sql_data)

   session_id_match = cursor.fetchone()

   if session_id_match != session_id_hmac:
      return false

   sql_data = "SELECT anti_csrf_token_hmac from users WHERE anti_csrf_token_hmac=\'" + anti_csrf_token_hmac + "\'"
   
   cursor.execute(sql_data)

   anti_csrf_token_hmac_match = cursor.fetchone()

   if anti_csrf_token_hmac_match != anti_csrf_token_hmac:
      return false

   return true

# Takes in message 'arg' and secret 'key' both of class 'str' and outputs base64-encoded 'str'

def hmac_message(arg,key):

   hmac = pysodium.crypto_generichash_blake2b_salt_personal(arg,outlen = pysodium.crypto_generichash_blake2b_BYTES,key = key,salt=b'0000000000000000',personal=b'0000000000000000')

#   print(type(hmac))

#   print(hmac)

#b64encode takes in 'bytes' and outputs 'bytes'

   base64_hmac = base64.b64encode(hmac).decode()

   return base64_hmac

#   print(type(base64_hmac))

#   print(base64_hmac)

